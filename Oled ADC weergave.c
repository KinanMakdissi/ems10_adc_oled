#include <msp430.h> 
#include <stdint.h>
int tiendegraden;
/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */
void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan.
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);
/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
int graden(int ADC_waarde)
{
    float spanning = (ADC_waarde * 1.5)/(1023.0);//1.5=Vref++
    int tiendegraden = spanning * 1000;
    return tiendegraden;
}

void setTitle(char tekst[]);


#pragma vector = ADC10_VECTOR
__interrupt void ADCOUT(void)
{
    static int tiendegraden;
    int ADCOUTPUT = ADC10MEM;
    tiendegraden = graden(ADCOUTPUT);
    setTemp(tiendegraden);
    //ADC10CTL0 &= ~(ADC10IFG); Automaticlly is reset hoef dus niet
}

#pragma vector =  TIMER0_A1_VECTOR;
__interrupt void startconversion(void)
{
    ADC10CTL0 |= ADC10SC;
    TA0CTL &= ~TAIFG;
}


int main(void)
{
	WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer
	
	// Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;
    //Timer start conversion insteling;
    TA0CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;
    TA0CCR0 = 55000;

    // ADC insteling;
    ADC10CTL1 = (INCH_5 | ADC10DIV_7 | ADC10SSEL_0 | SHS_0);
    ADC10CTL0 |= (SREF_1 | ADC10SHT_0 | REFON | ADC10ON | ADC10SR |  REFBURST | ADC10IE);
    ADC10CTL0 |= ENC;
	initDisplay();
	setTitle("Opdracht7.1.13");


	//uint8_t temperatuur = 0; // Fictieve temperatuur
	__enable_interrupt();

	while(1)
	{
//	    ADC10CTL0 |= ADC10SC;
//	    __delay_cycles(16000000);
	    // Hoog fictieve temperatuur op met 0.1 graad
	    // Geef dit weer op het display
	    //setTemp(temperatuur++);
	    // Niet te snel...
	}
}
